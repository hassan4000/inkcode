<?php

namespace App\Http\Controllers\Backend\Product;

use App\Http\Controllers\BaseController;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends BaseController
{
    public function index()
    {
        try {
            $data = Product::query()->where('is_deleted', 0)->get();

            return view('backend.product.index', compact('data'));
        } catch (\Exception $ex) {
            return redirect()->back()->withErrors($ex->getMessage());
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            $main_cats = Category::query()->where('is_deleted', 0)->get();
            $barnds = Brand::query()->where('is_deleted', 0)->get();

            return view('backend.product.create', compact('main_cats', 'barnds'));
        } catch (\Exception $ex) {
            return redirect()->back()->withErrors($ex->getMessage());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $rules = [
                'name_en' => 'required|string|max:191',
                'name_ar' => 'required|string|max:191',
                'main_price' => 'required|numeric',
                'brand_id' => 'required',

                'category_id' => 'required',
            ];
            $res_validate = $this->checkRequest($request, $rules);
            if (!$res_validate['status']) {
                return redirect()->back()->withErrors($res_validate['messages']);
            }
            $dataArr = $request->only([
                'name_en',
                'name_ar',
                'main_price',
                'brand_id',
                'category_id',
            ]);

            if (!is_null($request->image)) {
                $dataArr['media_path'] = $this->uploadPhoto($request, 'products', 'image');
            }
            DB::beginTransaction();

            $data = Product::query()->create($dataArr);

            DB::commit();

            return  redirect()->back()->withStatus(__('custom_messages.general_messages.crud.created'));
        } catch (\Exception $ex) {
            DB::rollBack();

            return redirect()->back()->withErrors($ex->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    public function edit($id)
    {
        try {
            $data = Product::query()->find($id);

            return view('backend.brand.edit', compact('data'));
        } catch (\Exception $ex) {
            return redirect()->back()->withErrors($ex->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $gift_id = $id;
            $gift = Product::query()->find($gift_id);
            $rules = [
                'name_en' => 'required|string|max:191',
                'name_ar' => 'required|string|max:191',
            ];
            $res_validate = $this->checkRequest($request, $rules);
            if (!$res_validate['status']) {
                return redirect()->back()->withErrors($res_validate['messages']);
            }
            $gift->name_en = $request->name_en;
            $gift->name_ar = $request->name_ar;
            if (!is_null($request->image)) {
                $gift->media_path = $this->uploadCategoryPhoto($request);
            }

            $gift->save();

            return redirect(route('gift.index'))
                ->withStatus(__('custom_messages.general_messages.crud.updated'));
        } catch (\Exception $ex) {
            return redirect()->back()->withErrors($ex->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *

     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $gift = Product::query()->find($id);
            $gift->update([
                'is_deleted' => 1,
            ]);

            return  redirect()->back()->withStatus(__('custom_messages.general_messages.crud.deleted'));
        } catch (\Exception $ex) {
            return redirect()->back()->withErrors($ex->getMessage());
        }
    }

    public function getViewSpecialProduct()
    {
        $data = SpecialProdoct::query()->first();
        $gifts = Product::where('is_deleted', 0)->get();

        return view('backend.product.create_sp', ['data' => $data, 'fks' => $gifts]);
    }

    public function storeSpecialProduct(Request $request)
    {
        try {
            $gift = SpecialProdoct::query()->delete();
            $dataArray = [
                'text_en' => $request->text_en,
                'text_ar' => $request->text_ar,
                'gift_id' => $request->gift_id,
                'title_ar' => $request->title_ar,
                'title_en' => $request->title_en,
            ];
            if (!is_null($request->image)) {
                $dataArray['media_path'] = $this->uploadCategoryPhoto($request);
            }
            $data = SpecialProdoct::query()->create($dataArray);

            return redirect(route('gift.getViewSpecialProduct'))
            ->withStatus(__('custom_messages.general_messages.crud.created'));
        } catch (\Exception $ex) {
            return redirect()->back()->withErrors($ex->getMessage());
        }
    }
}
