<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class BaseController extends Controller
{
    public function sendToken($token, $code = 200)
    {
        $response = [
            'status' => true,
            'token' => $token,
            'code' => $code
        ];
        return response()->json($response, $code);
    }

    public function sendResponseWithoutMessage($result, $code = 200)
    {
        $response = [
            'status' => true,
            'data' => $result,
            'code' => $code
        ];
        return response()->json($response, $code);
    }

    public function sendResponseWithMessage($result, $message, $code = 200)
    {
        $response = [
            'status' => true,
            'data' => $result,
            'messages' => $message,
            'code' => $code
        ];
        return response()->json($response, $code);
    }

    public function customErrorsMessages(array $arrayErrors): array
    {
        $errors = [];
        foreach ($arrayErrors as $key => $item) {

            array_push($errors, $item[0]);
        }
        return $errors;
    }

    public function sendError($errorMessage = [], $code = 404)
    {
        $response = [
            'status' => false,
        ];
        if (is_array($errorMessage)) {
            foreach ($errorMessage as $msg => $item) {
                $response[$msg] = implode($item);
            }
        }
        else{
            $response['message'] = $errorMessage;
        }
        $response['code'] = $code;
        return response()->json($response, $code);
    }

    public function uploadPersonalPhoto(Request $request)
    {
        if ($request->hasFile('image')) {
            $path = '/images/users/' . time() . $request->file('image')->getClientOriginalName();
            $request->file('image')->move('images/users/', $path);
        }
        return $path ?? null;
    }

    public function uploadCategoryPhoto(Request $request)
    {
        if ($request->hasFile('image')) {
            $path = '/images/categories/' . time() . $request->file('image')->getClientOriginalName();
            $request->file('image')->move('images/categories/', $path);
        }
        return $path ?? null;
    }
    public function uploadPhoto(Request $request,$folder='default',$fileName)
    {
        if ($request->hasFile('image')) {
            $path = "/images/$folder/" . time() . $request->file($fileName)->getClientOriginalName();
            $request->file($fileName)->move("images/$folder/", $path);
        }
        return $path ?? null;
    }
    public function uploadPhotoFromArray(Request $request,$folder='default',$fileName,$index)
    {
        if ($request->hasFile($fileName)) {
            $path = "/images/$folder/" . time() . $request->file($fileName)[$index]->getClientOriginalName();
            $request->file($fileName)[$index]->move("images/$folder/", $path);
        }
        return $path ?? null;
    }
    public function uploadSliderPhoto(Request $request)
    {
        if ($request->hasFile('image')) {
            $path = '/images/slider/' . time() . $request->file('image')->getClientOriginalName();
            $request->file('image')->move('images/categories/', $path);
        }
        return $path ?? null;
    }

    public function uploadDocumentPhoto(Request $request)
    {
        if ($request->hasFile('document')) {
            $path = '/documents/users/' . time() . $request->file('document')->getClientOriginalName();
            $request->file('document')->move('documents/users/', $path);
        }
        return $path ?? null;
    }

    public function uploadRequestMedia($image)
    {
        if (!is_null($image)) {
            $path = '/images/requestMedia/' . time() . $image->getClientOriginalName();
            $image->move('images/requestMedia/', $path);
        }
        return $path ?? null;
    }

    public function uploadAppLogo($logo)
    {
        if (!is_null($logo)) {
            $path = '/images/app/logo/' . time() . $logo->getClientOriginalName();
            $logo->move('images/app/logo/', $path);
        }
        return $path ?? null;

        if (!is_null($image)) {
            $path = '/images/requestMedia/' . time() . $image->getClientOriginalName();
            $image->move('images/requestMedia/', $path);
        }
        return $path ?? null;
    }

    public function changeLang($lang)
    {
        if ($lang === 'ar') {
            App::setLocale('ar');
        } else {
            App::setLocale('en');
        }
    }

    public function changeLangWeb(Request $request)
    {
        isset($request->lang_name) ? $lang = $request->lang_name : $lang = 'en';
        Session::put('locale', $lang);
        App::setLocale($lang);
        return redirect()->back();
    }

    public function checkRequest(Request $request, array $rules): array
    {
        $status = true;
        $msgs = null;
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $status = false;
            $msgs = $validator->getMessageBag();
        }
        return ['status' => $status, 'messages' => $msgs];
    }
}
