<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Traits\GeneralFunction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    use GeneralFunction;

    public function __construct()
    {
        $this->changeLang(\request('lang'));
    }

    public function register(Request $request)
    {
        try {
            $rules = [
                'name' => 'required|string',
                'gender' => 'required|string',
                'phone_number' => 'required|int|unique:users',
                'country_code' => 'required|string',
                'password' => 'required|string|min:6',
            ];
            $vr = $this->validateRequest($request, $rules);
            if ($vr['status'] === false) {
                return $this->sendErrors($vr['errors'], 400);
            }

            $user = new User([
                'name' => $request->name,
                'phone_number' => $request->phone_number,
                'country_code' => $request->country_code,
                'password' => Hash::make($request->password),
                'gender' => $request->gender,
            ]);
            $user->save();
            $user2 = User::where('id', $user->id)->first();
            $tokenResult = $user->createToken('Personal Access Token');
            $token = $tokenResult->token;
            $token->expires_at = Carbon::now()->addMonths(4);

            if ($request->remember_me) {
                $token->expires_at = Carbon::now()->addMonths(12);
            }
            $token->save();

            return response()->json([
                'access_token' => $tokenResult->accessToken,
                'token_type' => 'Bearer',
                'expires_at' => Carbon::parse(
                    $tokenResult->token->expires_at
                )->toDateTimeString(),
                 'user' => $user2,
            ]);
        } catch (\Exception $ex) {
            return $this->sendErrors(__('custom_messages.general.internal_server_error'), 500);
        }
    }

    public function login(Request $request)
    {
        try {
            $this->changeLang(\request('lang'));
            $rules = [
                'phone_number' => 'required|int',
                'country_code' => 'required|string',
                'password' => 'required|string',
            ];
            $vr = $this->validateRequest($request, $rules);
            if ($vr['status'] === false) {
                return $this->sendErrors($vr['errors'], 400);
            }
            $credentials = $request->only(['country_code', 'phone_number', 'password']);
            if (!Auth::attempt($credentials)) {
                return response()->json(['status' => false,
                    'message' => trans('auth.failed'),
                ], 401);
            }
            $user = $request->user();
            $tokenResult = $user->createToken('Personal Access Token');
            $token = $tokenResult->token;
            $token->expires_at = Carbon::now()->addMonths(4);
            $token->save();

            return response()->json([
                'access_token' => $tokenResult->accessToken,
                'token_type' => 'Bearer',
                'expires_at' => Carbon::parse(
                    $tokenResult->token->expires_at
                )->toDateTimeString(),
                'user' => $user,
            ]);
        } catch (\Exception $ex) {
            return $this->sendErrors(__('auth.failed'), 500);
        }
    }

    public function loginAdmin(Request $request)
    {
        try {
            $this->changeLang(\request('lang'));
            $rules = [
                'email' => 'email ',
                'password' => 'required|string',
            ];
            $vr = $this->validateRequest($request, $rules);
            if ($vr['status'] === false) {
                return $this->sendErrors($vr['errors'], 400);
            }
            $credentials = $request->only(['email', 'password']);
            if (!Auth::attempt($credentials)) {
                return response()->json(['status' => false,
                    'message' => trans('auth.failed'),
                ], 401);
            }
            $user = $request->user();
            $tokenResult = $user->createToken('Personal Access Token');
            $token = $tokenResult->token;
            $token->expires_at = Carbon::now()->addMonths(4);
            $token->save();

            return response()->json([
                'access_token' => $tokenResult->accessToken,
                'token_type' => 'Bearer',
                'expires_at' => Carbon::parse(
                    $tokenResult->token->expires_at
                )->toDateTimeString(),
            ]);
        } catch (\Exception $ex) {
            return $this->sendErrors(__('auth.failed'), 500);
        }
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();

        return response()->json([
            'message' => 'Successfully logged out',
        ]);
    }

    public function user(Request $request)
    {
        try {
            $user = $request->user();

            return $this->sendSuccessResponse($user);
        } catch (\Exception $ex) {
            return $this->sendErrors(__('custom_messages.general.internal_server_error'), 500);
        }
    }

    public function updateLocation(Request $request)
    {
        try {
            $code = 200;
            $rules = [
                'lat' => 'required',
                'long' => 'required',
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $code = 400;

                return $this->sendError($validator->errors()->messages(), $code);
            }
            $user = $request->user();
            $user->lat = $request->lat;
            $user->long = $request->long;
            $res = $user->save();

            return $this->sendResponseWithMessage($res, __('custom_messages.general_messages.location_updated'), $code);
        } catch (\Exception $ex) {
            $code = 500;

            return $this->sendError($ex->getMessage(), $code);
        }
    }

    public function editInformation(Request $request)
    {
        try {
            $code = 200;
            $user_id = $request->user()->id;
            $rules = [
                'name' => 'required|string|max:191',
                'phone' => 'required|unique:users,phone,'.$user_id,
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $code = 400;

                return $this->sendErrors($validator->errors()->messages(), $code);
            }
            $user = $request->user();
            if ($request->hasFile('image')) {
                $user->image = $this->uploadPersonalPhoto($request);
            }
            $user->name = $request->name;
            $user->phone = $request->phone;
            if ($request->has('email')) {
                $user->email = $request->email;
            }
            $res = $user->save();

            return $this->sendResponseWithMessage($res, __('custom_messages.general_messages.information_updated'), $code);
        } catch (\Exception $ex) {
            $code = 500;

            return $this->sendError($ex->getMessage(), $code);
        }
    }

    public function social(Request $request)
    {
        try {
            $data = [
                'name' => $request->name,
                'phone_number' => $request->phone_number,
                'country_code' => 'social',
                'social_token' => $request->social_token,
                'password' => Hash::make('p@ssw0rd_cadue'),
                'gender' => 'male',
            ];
            $user = User::query()->updateOrCreate(['social_token' => $data['social_token']], $data);
            $user->phone_number = (float) $user->phone_number;
            $tokenResult = $user->createToken('Personal Access Token');
            $token = $tokenResult->token;
            $token->expires_at = Carbon::now()->addMonths(4);

            if ($request->remember_me) {
                $token->expires_at = Carbon::now()->addMonths(12);
            }
            $token->save();

            return response()->json([
                'access_token' => $tokenResult->accessToken,
                'token_type' => 'Bearer',
                  'user' => $user,

                'expires_at' => Carbon::parse(
                    $tokenResult->token->expires_at
                )->toDateTimeString(),
            ]);
        } catch (\Exception $ex) {
            return $this->sendErrors(__('custom_messages.general.internal_server_error'), 500);
        }
    }

    public function checkData(Request $request)
    {
        try {
            $dataToCheck = ['email' => null, 'phone' => null];
            $code = 200;
            if (isset($request->email)) {
                $is_exist = User::query()->where('email', $request->email)->count();
                if ($is_exist > 0) {
                    $dataToCheck['email'] = false;
                    $code = 400;
                } else {
                    $dataToCheck['email'] = true;
                }
            }

            if (isset($request->country_code)) {
                $is_exist = User::query()->where('country_code', $request->country_code)->where('phone_number', $request->phone_number)->count();
                if ($is_exist > 0) {
                    $dataToCheck['phone'] = false;
                    $code = 400;
                } else {
                    $dataToCheck['phone'] = true;
                }
            }

            return $this->sendSuccessResponse($dataToCheck, $code);
        } catch (\Exception $ex) {
            $code = 500;

            return $this->sendErrors($ex->getMessage(), $code);
        }
    }

    public function updatePassword(Request $request)
    {
        try {
            $rules = [
                'old_password' => 'required|min:6',
                'new_password' => 'required|min:6',
            ];
            $vr = $this->validateRequest($request, $rules);
            if ($vr['status'] === false) {
                return $this->sendErrors($vr['errors'], 400);
            }
            $user = \auth()->user();
            if (Hash::check($request->old_password, $user->password)) {
                $user->password = Hash::make($request->new_password);
                $user->save();
            }

            return $this->sendSuccessResponse($user, 200);
        } catch (\Exception $ex) {
            return $this->sendSuccessResponse(__('custom_messages.user.password.changed_un_successfully'), 500);
        }
    }

    public function updateInfo(Request $request)
    {
        $rules = [
            'name' => 'required',
            'email' => 'email|unique:users,id,'.\auth()->user()->id,
            'country_code' => 'required',
            'phone_number' => 'required',
          ];
        $vr = $this->validateRequest($request, $rules);
        if ($vr['status'] === false) {
            return $this->sendErrors($vr['errors'], 400);
        }

        $data = $request->only([
            'name',
            'email',
            'country_code',
            'phone_number',
            'gender',
            'date_of_birth',
        ]);
        $data['user_id'] = \auth()->user()->id;
        \auth()->user()->update($data);
        $res = User::query()->find(\auth()->user()->id);

        return $this->sendSuccessResponse($res, 200);
    }
}
