<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    use HasFactory;
    protected $fillable = [
        "order_number",
        "status",
        "total_cost",
        "client_id",
    ];
    protected $appends = [
        "final_total"
    ];
     


    protected $with = [
        "items",
        "items.product"
    ];


    public function items(){
        return $this->hasMany(OrderItems::class,'order_id','id');
    }

    public function owner(){
        return $this->belongsTo(User::class,'client_id','id');
    }


    public static function getUserOrder($user_id): Model
    {
        //check if user has order or create
        $order = self::query()->where('client_id', $user_id)->where('status', 'start')
        ->first();
        if(is_null($order)){
            $order = self::query()->create(['client_id' => $user_id, 'status' => 'start',"order_number"=>time()]);

        }


        return  $order;
    }

    public function getFinalTotalAttribute(){
        $total = 0;
            foreach ($this->items as $item) {
                if(!is_null($item->product)){
                    $total+=$item->product->main_price;
                }
            }
            return $total;
    }


    public static function doneOrder($order)
    {


        if(!is_null($order)){
            $order->update(["status"=>"done","total_cost"=>$order->final_total]);
            return  $order;
        }
        return  null;
    }


}
