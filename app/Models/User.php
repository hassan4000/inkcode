<?php

namespace App\Models;

use Illuminate\Auth\MustVerifyEmail as MustVerifyEmailTrait;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;
use App\Models\UserToken\UserToken;

class User extends Authenticatable
{
    use HasFactory, Notifiable,HasApiTokens,HasRoles;




    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'user_type',
        'gender',
        'password',
        'account_status',
        'country_code',
        'phone_number',
        'date_of_birth',
        'social_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'is_deleted',
        'remember_token',
    ];
   
    public function deleteFcmUserToken()
    {
        return UserToken::where('user_id',request()->user()->id)->delete();
    }


    public static function getTokens($ids) : array
    {
        if(!is_array($ids)) {
            $ids = [$ids];
        }
        return UserToken::whereIn('user_id',$ids)->pluck('token')->toArray();
    }
}
