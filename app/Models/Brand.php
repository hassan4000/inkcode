<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    use HasFactory;

    protected $fillable = [
        'number',
        'media_path',
        'name_ar',
        'name_en',
        'is_deleted',
    ];
}
