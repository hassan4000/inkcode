<?php

namespace App\Traits;

   use Illuminate\Http\Request;
   use Illuminate\Support\Facades\App;
   use Illuminate\Support\Facades\Validator;

trait GeneralFunction
{
    /**
     * change Language.
     *
     * @param $lang
     */
    public function changeLang($lang)
    {
        if ($lang === 'ar') {
            App::setLocale('ar');
        } else {
            App::setLocale('en');
        }
    }

    /**
     * get local language.
     */
    public function getLang(): string
    {
        return App::getLocale();
    }

    public function getInformationByUserType($type, $user_id)
    {
        $type = strtolower($type);
        $info = auth()->user();

        return $info;
    }

    public function customErrorsMessages(array $arrayErrors): array
    {
        $errors = [];
        foreach ($arrayErrors as $key => $item) {
            array_push($errors, $item[0]);
        }

        return  $errors;
    }

    public function sendErrors($messages, $code = 400)
    {
        if (is_array($messages)) {
            $messages = $this->customErrorsMessages($messages);
        }

        return response()->json(['status' => false, 'message' => $messages], $code);
    }

    public function sendSuccessResponse($data, $code = 200, $msg = null)
    {
        $res = ['status' => true, 'data' => $data];
        if (!is_null($msg)) {
            $res['message'] = $msg;
        }

        return response()->json($res, $code);
    }

    public function validateRequest(Request $request, array $rules)
    {
        $data['status'] = true;
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $data['errors'] = $validator->errors()->getMessages();
            $data['status'] = false;
        } else {
            $data['errors'] = [];
            $data['status'] = true;
        }

        return $data;
    }
}
