<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();

            $table->string('name_ar')->nullable();
            $table->string('name_en')->nullable();
            $table->string('media_path')->nullable();
            $table->decimal('main_price', 8, 2)->default(0.00);
            $table->unsignedBigInteger('brand_id')->nullable();
            $table->unsignedBigInteger('category_id')->nullable();
            $table->boolean('is_deleted')->default(0);

            $table->foreign('category_id')
                ->references('id')->on('categories')->onDelete('cascade');

            $table->foreign('brand_id')
                ->references('id')->on('brands')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gifts');
    }
}
