<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::query()->create([
            'name' => 'Admin ',
            'country_code' => '+963',
            'phone_number' => '951753258',
            'email' => 'admin@admin.com',
            'user_type' => 'admin',
            'account_status' => 'active',
            'password' => Hash::make('admin'),
        ]);
        $admin_role = \Spatie\Permission\Models\Role::query()->where('name', 'admin')->get();
        $admin->assignRole($admin_role);
    }
}
