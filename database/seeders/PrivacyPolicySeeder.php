<?php

namespace Database\Seeders;

use App\Models\PrivacyPolicy\PrivacyPolicy;
use Illuminate\Database\Seeder;

class PrivacyPolicySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $privacyPolicy = [
            [
                'title' => 'Privacy Policy',
                'description' => '<h2>General Information</h2>
                                <p>Welcome to our privacy policy page!<br>When you use our website , you give us full confidence in your information .<br>This Privacy Policy will help you to understand and know what information we collect ,<br>
                                        why we collect it, and what we do with it, you help us to better serve you.<br><br></p>
                                <h2>What information do we collect?<br></h2>
                                <p>we collect the information you inter when you register on our site,<br>when you place an order, subscribe to our monthly newsletters,<br>
                                        respond to a questionnaire or complete our form.<br></>
                                <h2>How do we protect your information?</h2>
                                <p>we implement a different set of safeguards to ensure the security of your personal information<br>when you place an order, or when you enter your personal information and when you try to access it.<br>
                                        We use secure.</p>
                                <p>&nbsp;</p>',
                'lang' => 'en',
                'created_at' => now(), 'updated_at' => now()],
            [
                'title' => 'سياسة الخصوصية',
                'description' => '<h2 style="text-align: right;">معلومات عامة</h2>
                                <p style="text-align: right;">مرحبًا بك في صفحة سياسة الخصوصية الخاصة بنا!<br />عندما تستخدم موقعنا ، فإنك تمنحنا الثقة الكاملة في معلوماتك.<br />ستساعدك سياسة الخصوصية هذه على فهم ومعرفة المعلومات التي نجمعها ،<br />لماذا نجمعها ، وماذا نفعل بها ، أنت تساعدنا على خدمتك بشكل أفضل.</p>
                                <h2 style="text-align: right;">ما هي المعلومات التي نقوم بجمعها؟</h2>
                                <p style="text-align: right;">نقوم بجمع المعلومات التي تدخلها عند التسجيل على موقعنا ،<br />عند تقديم طلب ، اشترك في نشراتنا الإخبارية الشهرية ،<br />الرد على استبيان أو إكمال النموذج الخاص بنا.</p>
                                <h2 style="text-align: right;">كيف نحمي معلوماتك؟</h2>
                                <p style="text-align: right;">نطبق مجموعة مختلفة من الضمانات لضمان أمان معلوماتك الشخصية<br />عند تقديم طلب ، أو عند إدخال معلوماتك الشخصية وعندما تحاول الوصول إليها.<br />نحن نستخدم آمنة.</p>',
                'lang' => 'ar',
                'created_at' => now(), 'updated_at' => now()],

        ];
        PrivacyPolicy::query()->insert($privacyPolicy);
    }
}
