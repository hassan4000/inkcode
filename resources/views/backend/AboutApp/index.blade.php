@extends('master_layouts.app')

@section('content_head')
    @include('master_layouts.includes.menu_content_head',['title_page'=>__('title_page.about_app')])
@endsection
@section('content')
    <div class="card card-custom card-sticky" id="kt_page_sticky_card">
        <div class="">
            @include('includes.errors')
        </div>
        <div class="">
            @include('includes.success')
        </div>
        <div class="card-header" style="">
            <div class="card-toolbar row">
                <div class="col-2"></div>
                <div class="col-8 d-flex justify-content-center">
                    <a href="{{route('aboutApp.edit')}}" class="btn btn-label-brand btn-bold font-weight-bold ">
                        <i class="flaticon2-edit icon-sm"></i>{{__('dashboard.edit')}}
                    </a>
                </div>
                <div class="col-2"></div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
                @if(!is_null($about_ar) && !is_null($about_ar))
                    <form enctype="multipart/form-data" class="form" id="kt_form" method="POST"
                          action="">
                        @csrf
                        <div class="kt-form kt-form--label-right">
                            <div class="kt-form__body">
                                <div class="kt-section kt-section--first">
                                    <div class="kt-section__body">
                                        <div class="form-group row">
                                            <div class="col-3"></div>
                                            <div class="col-6 d-flex justify-content-center">
                                                <img style="width: 300px;"
                                                     src="{{$about_ar->logo}}" alt="">
                                            </div>
                                            <div class="col-3"></div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-3"></div>
                                            <div class="col-6 d-flex justify-content-center">
                                                <h5>{{$about_en->email}}</h5>
                                            </div>
                                            <div class="col-3"></div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-3"></div>
                                            <div class="col-6 d-flex justify-content-center">
                                                <h5 style="color: #62ceeb">Version {{$about_en->app_version}}</h5>
                                            </div>
                                            <div class="col-3"></div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-2"></div>
                                            <div class="col-8 d-flex justify-content-center">
                                                <p>
                                                    @if(\Illuminate\Support\Facades\Session::get('locale')=='en')
                                                        {{$about_en->information}}
                                                    @endif
                                                    @if(\Illuminate\Support\Facades\Session::get('locale')=='ar'&& !is_null($about_ar))
                                                        {{$about_ar->information}}
                                                    @endif
                                                </p>
                                            </div>
                                            <div class="col-2"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                @endif
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="{{asset('wjez/js/user_table.js')}}"></script>
    <script src="{{asset('assets/js/pages/custom/user/edit-user.js')}}"></script>
@endsection

