@extends('master_layouts.app')
@section('style')
@endsection
@section('content_head')
    @include('master_layouts.includes.menu_content_head',['title_page'=>__('title_page.users_management')])
@endsection
@section('content')
    <div class="card card-custom card-sticky" id="kt_page_sticky_card">
        <div class="">
            @include('includes.errors')
        </div>
        <div class="">
            @include('includes.success')
        </div>

        <div id="local_lang" hidden>{{\Illuminate\Support\Facades\Session::get('locale')}}</div>

        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
            <div class="kt-portlet kt-portlet--tabs">
                <div class="kt-portlet__body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="kt_user_edit_tab_1" role="tabpanel">
                            <form enctype="multipart/form-data" class="form" id="kt_form" method="POST"
                                  action="{{route('users.store')}}">
                                @csrf
                                <div class="kt-form kt-form--label-right">
                                    <div class="kt-form__body">
                                        <div class="kt-section kt-section--first">
                                            <div class="kt-section__body">
                                                {{--provider info--}}
                                                <div class="row">
                                                    <label class="col-xl-3"></label>
                                                    <div class="col-lg-7 col-xl-6">
                                                        <h3 class="kt-section__title kt-section__title-sm">{{__('dashboard.users.provider_info')}}
                                                            :</h3>
                                                    </div>
                                                    <label class="col-xl-2"></label>
                                                </div>
                                                {{--image--}}
                                                <div class="form-group row">
                                                    <label class="col-4"></label>
                                                    <div class="col-lg-8 col-xl-6">
                                                        <div class="kt-avatar kt-avatar--outline  kt-avatar--circle"
                                                             id="kt_user_edit_avatar">
                                                            <div class="kt-avatar__holder"
                                                                 style="background-image: url('{{asset('assets/media/users/default.jpg')}}');"
                                                            ></div>
                                                            <label class="kt-avatar__upload" data-toggle="kt-tooltip"
                                                                   title="" data-original-title="Change image">
                                                                <i class="fa fa-pen"></i>
                                                                <input type="file" name="image"
                                                                       accept=".png, .jpg, .jpeg">
                                                            </label>
                                                            <span class="kt-avatar__cancel" data-toggle="kt-tooltip"
                                                                  title="" data-original-title="Cancel image">
																				<i class="fa fa-times"></i>
																			</span>
                                                        </div>
                                                    </div>
                                                    <label class="col-xl-2"></label>
                                                </div>
                                                {{--name--}}
                                                <div class="form-group row">
                                                    <div class="col-1"></div>
                                                    <label
                                                        class="col-xl-2 col-lg-2 col-form-label">{{__('dashboard.full_name')}}</label>
                                                    <div class="col-lg-6 col-xl-6">
                                                        <input required
                                                               placeholder="{{__('dashboard.placeholder.name')}}"
                                                               name="name"
                                                               class="form-control" type="text" value="">
                                                    </div>
                                                    <label class="col-xl-3"></label>
                                                </div>
                                                {{--user type--}}
                                                <div class="form-group row">
                                                    <div class="col-1"></div>
                                                    <label
                                                        class="col-xl-2 col-lg-2 col-form-label">{{__('dashboard.user_type')}}</label>
                                                    <div class="col-lg-6 col-xl-6">
                                                        <select name="user_type"
                                                                class="form-control form-control-solid">
                                                            <option selected
                                                                    value="provider">{{__('dashboard.users.provider')}}</option>
                                                        </select>
                                                    </div>
                                                    <label class="col-xl-3"></label>
                                                </div>
                                                {{--phone--}}
                                                <div class="form-group row">
                                                    <div class="col-1"></div>
                                                    <label
                                                        class="col-xl-2 col-lg-2 col-form-label">{{__('dashboard.phone')}}</label>
                                                    <div class="col-lg-6 col-xl-6">
                                                        <div class="input-group input-group-solid">
                                                            <div class="input-group-prepend"><span
                                                                    class="input-group-text"><i class="la la-phone"></i></span>
                                                            </div>
                                                            <input required type="text" class="form-control"
                                                                   value="" name="phone"
                                                                   placeholder="{{__('dashboard.placeholder.phone')}}"
                                                                   aria-describedby="basic-addon1">
                                                        </div>
                                                        <span
                                                            class="form-text text-muted">{{__('custom_messages.general_messages.your_phone_is_private')}}</span>
                                                    </div>
                                                    <label class="col-xl-3"></label>
                                                </div>
                                                {{--password--}}
                                                <div class="form-group row">
                                                    <div class="col-1"></div>
                                                    <label
                                                        class="col-xl-2 col-lg-2 col-form-label">{{__('dashboard.password')}}</label>
                                                    <div class="col-lg-6 col-xl-6">
                                                        <input required
                                                               placeholder="{{__('dashboard.placeholder.password')}}"
                                                               name="password"
                                                               class="form-control" type="password" value="">
                                                    </div>
                                                    <label class="col-xl-3"></label>
                                                </div>
                                                {{--confirm password--}}
                                                <div class="form-group row">
                                                    <div class="col-1"></div>
                                                    <label
                                                        class="col-xl-2 col-lg-2 col-form-label">{{__('dashboard.c_password')}}</label>
                                                    <div class="col-lg-6 col-xl-6">
                                                        <input required
                                                               placeholder="{{__('dashboard.placeholder.c_password')}}"
                                                               name="c_password"
                                                               class="form-control" type="password" value="">
                                                    </div>
                                                    <label class="col-xl-3"></label>
                                                </div>
                                                {{--email--}}
                                                <div class="form-group row">
                                                    <div class="col-1"></div>
                                                    <label
                                                        class="col-xl-2 col-lg-2 col-form-label">{{__('dashboard.email')}}</label>
                                                    <div class="col-lg-6 col-xl-6 ">
                                                        <div class="input-group  input-group-solid">
                                                            <div class="input-group-prepend"><span
                                                                    class="input-group-text"><i
                                                                        class="la la-at"></i></span></div>
                                                            <input type="email" class="form-control" name="email"
                                                                   value="" placeholder="Email"
                                                                   aria-describedby="basic-addon1">
                                                        </div>
                                                    </div>
                                                    <label class="col-xl-3"></label>
                                                </div>
                                                {{--birthday--}}
                                                <div class="form-group row">
                                                    <div class="col-1"></div>
                                                    <label
                                                        class="col-xl-2 col-lg-2 col-form-label">{{__('dashboard.birthday')}}</label>
                                                    <div class="col-lg-6 col-xl-6 ">
                                                        <div class="input-group  input-group-solid">
                                                            <div class="input-group-prepend"><span
                                                                    class="input-group-text"><i
                                                                        class="la la-calendar"></i></span></div>
                                                            <input required type="date" class="form-control"
                                                                   name="birthday"
                                                                   value="" placeholder="birthday"
                                                                   aria-describedby="basic-addon1">
                                                        </div>
                                                    </div>
                                                    <label class="col-xl-3"></label>
                                                </div>
                                                {{--category--}}
                                                <div class="form-group row">
                                                    <div class="col-1"></div>
                                                    <label
                                                        class="col-xl-2 col-lg-2 col-form-label">{{__('dashboard.what_do_you_provide')}}</label>
                                                    <div class="col-lg-6 col-xl-6">
                                                        <select id="select_category" name="main_category_id"
                                                                data-role="select_category"
                                                                class="form-control form-control-solid">
                                                            <option selected disabled hidden
                                                                    value="{{null}}">{{__('dashboard.choose_service')}}</option>
                                                            {{$categories=\App\Models\Services\Category::query()->where('is_main',1)->get()}}
                                                            @foreach($categories as $cate)
                                                                <option style="background-image:url({{$cate->image}});"
                                                                        value="{{$cate->id}}">
                                                                    @if(\Illuminate\Support\Facades\Session::get('locale') =='ar')
                                                                        {{$cate->name_ar}}
                                                                    @endif
                                                                    @if(\Illuminate\Support\Facades\Session::get('locale') =='en')
                                                                        {{$cate->name}}
                                                                    @endif
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <label class="col-xl-3"></label>
                                                </div>
                                                {{--sub category--}}
                                                <div class="form-group row">
                                                    <div class="col-1"></div>
                                                    <label
                                                        class="col-xl-2 col-lg-2 col-form-label">{{__('dashboard.what_do_you_provide')}}</label>
                                                    <div class="col-lg-6 col-xl-6">
                                                        <select multiple id="select_sub_category" name="sub_category_id[]"
                                                                class="form-control form-control-solid">
                                                        </select>
                                                    </div>
                                                    <label class="col-xl-3"></label>
                                                </div>

                                                {{--<div class="form-group row">
                                                    <div class=" col-lg-4 col-md-9 col-sm-12" data-select2-id="38">
                                                        <span class="select2 select2-container select2-container--default select2-container--below select2-container--focus select2-container--open" dir="ltr" data-select2-id="9" style="width: 314.65px;">
                                                            <span class="selection">
                                                                <span class="select2-selection select2-selection--multiple" role="combobox" aria-haspopup="true" aria-expanded="true" tabindex="-1" aria-disabled="false" aria-owns="select2-kt_select2_3-results" aria-activedescendant="select2-kt_select2_3-result-krfa-AK">
                                                                    <ul class="select2-selection__rendered">
                                                                        <li class="select2-selection__choice" title="Alaska" data-select2-id="13">
                                                                            <span class="select2-selection__choice__remove" role="presentation">
                                                                                ×
                                                                            </span>Alaska
                                                                        </li><li class="select2-selection__choice" title="Nevada" data-select2-id="14">
                                                                            <span class="select2-selection__choice__remove" role="presentation">
                                                                                ×
                                                                            </span>Nevada
                                                                        </li>
                                                                        <li class="select2-selection__choice" title="Montana" data-select2-id="15">
                                                                            <span class="select2-selection__choice__remove" role="presentation">
                                                                                ×
                                                                            </span>Montana
                                                                        </li>
                                                                        <li class="select2-search select2-search--inline">
                                                                            <input class="select2-search__field" type="search" tabindex="0" autocomplete="off" autocorrect="off" autocapitalize="none" spellcheck="false" role="searchbox" aria-autocomplete="list" placeholder="" style="width: 0.75em;" aria-controls="select2-kt_select2_3-results" aria-activedescendant="select2-kt_select2_3-result-krfa-AK">
                                                                        </li>
                                                                    </ul>
                                                                </span>
                                                            </span>
                                                            <span class="dropdown-wrapper" aria-hidden="true"></span>
                                                        </span>
                                                    </div>
                                                </div>--}}

                                                {{--account status--}}
                                                <div class="form-group row">
                                                    <div class="col-1"></div>
                                                    <label
                                                        class="col-2">{{__('dashboard.account_status.account_status')}}</label>
                                                    <div class="col-lg-6 col-xl-6">
                                                        <select name="account_status"
                                                                class="form-control form-control-solid">
                                                            <option
                                                                value="active">{{__('dashboard.account_status.active')}}
                                                            </option>
                                                            <option
                                                                value="pending">{{__('dashboard.account_status.pending')}}
                                                            </option>
                                                        </select>
                                                    </div>
                                                    <label class="col-xl-3"></label>
                                                </div>
                                                {{--talkig lang--}}
                                                <div class="form-group row">
                                                    <div class="col-1"></div>
                                                    <label
                                                        class="col-xl-2 col-lg-2 col-form-label">{{__('dashboard.talking_lang')}}</label>
                                                    <div class="col-lg-6 col-xl-6">
                                                        <input required
                                                               placeholder="{{__('dashboard.placeholder.talking_lang')}}"
                                                               name="talking_lang"
                                                               class="form-control" type="text"
                                                               value="">
                                                    </div>
                                                    <label class="col-xl-3"></label>
                                                </div>
                                                {{--city--}}
                                                <div class="form-group row">
                                                    <div class="col-1"></div>
                                                    <label
                                                        class="col-xl-2 col-lg-2 col-form-label">{{__('dashboard.city')}}</label>
                                                    <div class="col-lg-6 col-xl-6">
                                                        <input
                                                            placeholder="{{__('dashboard.city')}}"
                                                            name="city"
                                                            class="form-control" type="text"
                                                            value="">
                                                    </div>
                                                    <label class="col-xl-3"></label>
                                                </div>
                                                {{--document id--}}
                                                <div class="form-group row">
                                                    <div class="col-1"></div>
                                                    <label
                                                        class="col-xl-2 col-lg-2 col-form-label">{{__('dashboard.document_id')}}</label>
                                                    <div class="col-lg-6 col-xl-6">
                                                        <input required
                                                               placeholder="{{__('dashboard.placeholder.document_id')}}"
                                                               name="document_id"
                                                               class="form-control" type="text"
                                                               value="">
                                                    </div>
                                                    <label class="col-xl-3"></label>
                                                </div>
                                                {{--document--}}
                                                <div class="form-group row">
                                                    <div class="col-1"></div>
                                                    <label
                                                        class="col-xl-2 col-lg-2 col-form-label">{{__('dashboard.document')}}</label>
                                                    <div class="col-lg-6 col-xl-6">
                                                        <div class="col-lg-9 col-xl-6">
                                                            <div class="kt-avatar kt-avatar--outline  kt-avatar--brand"
                                                                 id="kt_user_edit_document">
                                                                <div class="kt-avatar__holder"
                                                                     style=""></div>
                                                                <label class="kt-avatar__upload"
                                                                       data-toggle="kt-tooltip"
                                                                       title="" data-original-title="Change document">
                                                                    <i class="fa fa-pen"></i>
                                                                    <input type="file" name="document"
                                                                           accept=".png, .jpg, .jpeg"
                                                                           value="">
                                                                </label>
                                                                <span class="kt-avatar__cancel" data-toggle="kt-tooltip"
                                                                      title="" data-original-title="Cancel document">
																				<i class="fa fa-times"></i>
																			</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <label class="col-xl-3"></label>
                                                </div>
                                                {{--bio--}}
                                                <div class="form-group row">
                                                    <div class="col-1"></div>
                                                    <label
                                                        class="col-xl-2 col-lg-2 col-form-label">{{__('dashboard.bio')}}</label>
                                                    <div class="col-lg-6 col-xl-6">
                                                        <input required
                                                               placeholder="{{__('dashboard.placeholder.bio')}}"
                                                               name="bio"
                                                               class="form-control" type="text" value="">
                                                    </div>
                                                    <label class="col-xl-3"></label>
                                                </div>
                                            </div>

                                            <div
                                                class="kt-separator kt-separator--space-lg kt-separator--fit kt-separator--border-solid"></div>
                                            <div class="kt-form__actions">
                                                <div class="row d-flex justify-content-center">
                                                    <div class="col-xl-3"></div>
                                                    <div class="col-lg-6 col-xl-6">
                                                        <a href="{{route('users.index','provider')}}"
                                                           class="btn btn-clean btn-bold">
                                                            <i class="ki ki-long-arrow-back icon-sm"></i>{{__('dashboard.back')}}
                                                        </a>
                                                        <button type="submit"
                                                                class="btn btn-label-brand btn-bold font-weight-bold ">
                                                            <i class="ki ki-check icon-sm"></i>{{__('dashboard.users.add')}}
                                                        </button>
                                                    </div>
                                                    <label class="col-xl-3"></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('js')
    <script src="{{asset('wjez/js/ajaxRequest.js')}}" type="text/javascript"></script>
    <script src="{{asset('wjez/js/user_table.js')}}"></script>
    <script src="{{asset('assets/js/pages/custom/user/edit-user.js')}}"></script>
    <script src="{{asset('assets/js/pages/crud/forms/widgets/select2.js')}}" type="text/javascript"></script>
@endsection
