@extends('master_layouts.app')
    @include('master_layouts.includes.menu_content_head',['title_page'=>__('title_page.wraps_management')])


@section('style')
<link href="{{   asset('assets/plugins/custom/kanban/kanban.bundle.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

						<!-- begin:: Subheader -->
						<div class="kt-subheader   kt-grid__item" id="kt_subheader">
							<div class="kt-container  kt-container--fluid ">
								<div class="kt-subheader__main">
									<h3 class="kt-subheader__title">
										Kanban Board </h3>
									<span class="kt-subheader__separator kt-hidden"></span>
									<div class="kt-subheader__breadcrumbs">
										<a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
										<span class="kt-subheader__breadcrumbs-separator"></span>
										<a href="" class="kt-subheader__breadcrumbs-link">
											Components </a>
										<span class="kt-subheader__breadcrumbs-separator"></span>
										<a href="" class="kt-subheader__breadcrumbs-link">
											Extended </a>
										<span class="kt-subheader__breadcrumbs-separator"></span>
										<a href="" class="kt-subheader__breadcrumbs-link">
											Kanban Board </a>

										<!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
									</div>
								</div>
								<div class="kt-subheader__toolbar">
									<div class="kt-subheader__wrapper">
										<a href="#" class="btn kt-subheader__btn-primary">
											Actions &nbsp;

											<!--<i class="flaticon2-calendar-1"></i>-->
										</a>
										<div class="dropdown dropdown-inline" data-toggle="kt-tooltip" title="" data-placement="left" data-original-title="Quick actions">
											<a href="#" class="btn btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
												<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--success kt-svg-icon--md">
													<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
														<polygon points="0 0 24 0 24 24 0 24"></polygon>
														<path d="M5.85714286,2 L13.7364114,2 C14.0910962,2 14.4343066,2.12568431 14.7051108,2.35473959 L19.4686994,6.3839416 C19.8056532,6.66894833 20,7.08787823 20,7.52920201 L20,20.0833333 C20,21.8738751 19.9795521,22 18.1428571,22 L5.85714286,22 C4.02044787,22 4,21.8738751 4,20.0833333 L4,3.91666667 C4,2.12612489 4.02044787,2 5.85714286,2 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
														<path d="M11,14 L9,14 C8.44771525,14 8,13.5522847 8,13 C8,12.4477153 8.44771525,12 9,12 L11,12 L11,10 C11,9.44771525 11.4477153,9 12,9 C12.5522847,9 13,9.44771525 13,10 L13,12 L15,12 C15.5522847,12 16,12.4477153 16,13 C16,13.5522847 15.5522847,14 15,14 L13,14 L13,16 C13,16.5522847 12.5522847,17 12,17 C11.4477153,17 11,16.5522847 11,16 L11,14 Z" fill="#000000"></path>
													</g>
												</svg>

												<!--<i class="flaticon2-plus"></i>-->
											</a>
											<div class="dropdown-menu dropdown-menu-fit dropdown-menu-md dropdown-menu-right">

												<!--begin::Nav-->
												<ul class="kt-nav">
													<li class="kt-nav__head">
														Add anything or jump to:
														<i class="flaticon2-information" data-toggle="kt-tooltip" data-placement="right" title="" data-original-title="Click to learn more..."></i>
													</li>
													<li class="kt-nav__separator"></li>
													<li class="kt-nav__item">
														<a href="#" class="kt-nav__link">
															<i class="kt-nav__link-icon flaticon2-drop"></i>
															<span class="kt-nav__link-text">Order</span>
														</a>
													</li>
													<li class="kt-nav__item">
														<a href="#" class="kt-nav__link">
															<i class="kt-nav__link-icon flaticon2-calendar-8"></i>
															<span class="kt-nav__link-text">Ticket</span>
														</a>
													</li>
													<li class="kt-nav__item">
														<a href="#" class="kt-nav__link">
															<i class="kt-nav__link-icon flaticon2-telegram-logo"></i>
															<span class="kt-nav__link-text">Goal</span>
														</a>
													</li>
													<li class="kt-nav__item">
														<a href="#" class="kt-nav__link">
															<i class="kt-nav__link-icon flaticon2-new-email"></i>
															<span class="kt-nav__link-text">Support Case</span>
															<span class="kt-nav__link-badge">
																<span class="kt-badge kt-badge--success">5</span>
															</span>
														</a>
													</li>
													<li class="kt-nav__separator"></li>
													<li class="kt-nav__foot">
														<a class="btn btn-label-brand btn-bold btn-sm" href="#">Upgrade plan</a>
														<a class="btn btn-clean btn-bold btn-sm" href="#" data-toggle="kt-tooltip" data-placement="right" title="" data-original-title="Click to learn more...">Learn more</a>
													</li>
												</ul>

												<!--end::Nav-->
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

						<!-- end:: Subheader -->

						<!-- begin:: Content -->
						<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">




							<div class="row">
								<div class="col-lg-12">
									<div class="kt-portlet">
										<div class="kt-portlet__head">
											<div class="kt-portlet__head-label">
												<h3 class="kt-portlet__head-title">
													Dynamic Board &amp; Task Demo
												</h3>
											</div>
										</div>
										<div class="kt-portlet__body">
											<div id="kanban4"><div class="kanban-container" style="width: 750px;"><div data-id="_board1" data-order="1" class="kanban-board" style="width: 250px; margin-left: 0px; margin-right: 0px;"><header class="kanban-board-header"><div class="kanban-title-board">Board 1</div></header><main class="kanban-drag"><div class="kanban-item">My Task Test</div><div class="kanban-item">Buy Milk</div></main><footer></footer></div><div data-id="_board2" data-order="2" class="kanban-board" style="width: 250px; margin-left: 0px; margin-right: 0px;"><header class="kanban-board-header"><div class="kanban-title-board">Board 2</div></header><main class="kanban-drag"><div class="kanban-item">Do Something!</div><div class="kanban-item">Run?</div></main><footer></footer></div><div data-id="_board3" data-order="3" class="kanban-board" style="width: 250px; margin-left: 0px; margin-right: 0px;"><header class="kanban-board-header"><div class="kanban-title-board">Board 3</div></header><main class="kanban-drag"><div class="kanban-item">All right</div><div class="kanban-item">Ok!</div></main><footer></footer></div></div></div>
											<div class="kanban-toolbar">
												<div class="row">
													<div class="col-lg-4">
														<div class="kanban-toolbar__title">
															Add New Board
														</div>
														<div class="form-group row">
															<div class="col-12">
																<input id="kanban-add-board" class="form-control" type="text" placeholder="Board Name"><br>
																<select id="kanban-add-board-color" class="form-control">
																	<option value="">Select a Board Color</option>
																	<option value="brand">Brand</option>
																	<option value="brand-light">Brand Light</option>
																	<option value="primary">Primary</option>
																	<option value="primary-light">Primary Light</option>
																	<option value="success">Success</option>
																	<option value="success-light">Success Light</option>
																	<option value="info">Info</option>
																	<option value="info-light">Info Light</option>
																	<option value="warning">Warning</option>
																	<option value="warning-light">Warning Light</option>
																	<option value="danger">Danger</option>
																	<option value="danger-light">Danger Light</option>
																</select>
																<br>
																<button class="btn btn-success" id="addBoard">Add board</button>
															</div>
														</div>
													</div>
													<div class="col-lg-4">
														<div class="kanban-toolbar__title">
															Add New Task
														</div>
														<div class="form-group">
															<input id="kanban-add-task" class="form-control" type="text" placeholder="Task Name"><br>
															<select id="kanban-select-task" class="form-control">
																<option value="">Select a Board</option>
																<option value="_board1">Board 1</option>
																<option value="_board2">Board 2</option>
																<option value="_board3">Board 3</option>
															</select>
															<br>
															<select id="kanban-add-task-color" class="form-control">
																<option value="">Select a Task Color</option>
																<option value="brand">Brand</option>
																<option value="primary">Primary</option>
																<option value="success">Success</option>
																<option value="info">Info</option>
																<option value="warning">Warning</option>
																<option value="danger">Danger</option>
															</select>
															<br>
															<button class="btn btn-primary" id="addTask">Add Task</button>
														</div>
													</div>
													<div class="col-lg-4">
														<div class="kanban-toolbar__title">
															Remove Board
														</div>
														<div class="form-group row">
															<div class="col-12">
																<select id="kanban-select-board" class="form-control">
																	<option value="">Select a Board</option>
																	<option value="_board1">Board 1</option>
																	<option value="_board2">Board 2</option>
																	<option value="_board3">Board 3</option>
																</select>
																<br>
																<button class="btn btn-danger" id="removeBoard2">Remove Board</button>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

						<!-- end:: Content -->
					</div>
@endsection
@section('js')
    <script>
        var KTAppOptions = {
            "colors": {
                "state": {
                    "brand": "#5d78ff",
                    "dark": "#282a3c",
                    "light": "#ffffff",
                    "primary": "#5867dd",
                    "success": "#34bfa3",
                    "info": "#36a3f7",
                    "warning": "#ffb822",
                    "danger": "#fd3995"
                },
                "base": {
                    "label": [
                        "#c5cbe3",
                        "#a1a8c3",
                        "#3d4465",
                        "#3e4466"
                    ],
                    "shape": [
                        "#f0f3ff",
                        "#d9dffa",
                        "#afb4d4",
                        "#646c9a"
                    ]
                }
            }
        };
    </script>
		<script src="{{asset('assets/plugins/custom/kanban/kanban.bundle.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/js/pages/components/extended/kanban-board.js')}}" type="text/javascript"></script>
@endsection

