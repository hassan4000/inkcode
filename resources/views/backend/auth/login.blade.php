<!DOCTYPE html>
<html lang="en">
<head>
    <base href="../../../">
    <meta charset="utf-8"/>
    <title> {{__('title_page.login')}} </title>
    <meta name="description" content="Login page example">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">
    <link href="{{asset('assets/css/pages/login/login-6.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/plugins/global/plugins.bundle.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/css/style.bundle.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/css/skins/header/base/light.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/css/skins/header/menu/light.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/css/skins/brand/dark.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/css/skins/aside/dark.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/css/animate.min.css')}}" rel="stylesheet" type="text/css"/>
    <link rel="shortcut icon" href="{{asset('assets/media/company-logos/logo.png')}}"/>
    <style>


    </style>
</head>
<body
    class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">

<div class="kt-grid kt-grid--ver kt-grid--root">
    <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v6 kt-login--signin" id="kt_login">
        <div
            class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--desktop kt-grid--ver-desktop kt-grid--hor-tablet-and-mobile">
            <div class="kt-grid__item  kt-grid__item--order-tablet-and-mobile-2  kt-grid kt-grid--hor kt-login__aside">
                <div class="kt-login__wrapper">
                    <div class="kt-login__container">
                        <div class="kt-login__body">
                            <div class="kt-login__logo  wow swing center ">
                                <br>
                                <h1 class="font-weight-bolder" >INK Code TEST</h1>
                            </div>
                            <div class="kt-login__signin wow bounceInUp center">
                                <div class="">
                                    @include('includes.errors')
                                </div>

                                <div class="kt-login__form ">
                                    <form class="kt-form" role="form" method="POST" action="{{route('login')}}">
                                        @csrf
                                        <div class="form-group">
                                            @if(\Illuminate\Support\Facades\Session::get('locale') =='ar')
                                                <input style="text-align: right;" class="form-control " type="text"
                                                       placeholder="{{__('dashboard.email')}}" name="email"
                                                       autocomplete="off" required>
                                            @endif
                                            @if(\Illuminate\Support\Facades\Session::get('locale') =='en')
                                                <input style="text-align: left;" class="form-control " type="text"
                                                       placeholder="{{__('dashboard.email')}}" name="email"
                                                       autocomplete="off" required>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            @if(\Illuminate\Support\Facades\Session::get('locale') =='ar')
                                                <input style="text-align: right;" class="form-control form-control-last" type="password"
                                                       placeholder="{{__('dashboard.password')}}" name="password"
                                                       required>
                                            @endif
                                            @if(\Illuminate\Support\Facades\Session::get('locale') =='en')
                                                <input style="text-align: left;" class="form-control form-control-last" type="password"
                                                       placeholder="{{__('dashboard.password')}}" name="password"
                                                       required>
                                            @endif
                                        </div>

                                        <div class="kt-login__actions">
                                            <button type="submit" id="kt_login_signin_submit"
                                                    class="btn btn-warning btn-square btn-elevate  text-white text-capitalize">
                                                <span class="flaticon-signs" ></span>
                                                {{__('dashboard.sign_in')}}
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-grid__item kt-grid__item--fluid kt-grid__item--center kt-grid kt-grid--ver kt-login__content "
                 style="background-image: url({{asset('assets/media/bg/bg-4.jpg')}});">
                <div class="kt-login__section ">
                    <div class="kt-login__block"  >
                        <h3 class="kt-login__title cadue wow flipInX center"  > </h3>
                        <div class="kt-login__desc cadue wow bounceInRight center"  >

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var KTAppOptions = {
        "colors": {
            "state": {
                "brand": "#5d78ff",
                "dark": "#282a3c",
                "light": "#ffffff",
                "primary": "#5867dd",
                "success": "#34bfa3",
                "info": "#36a3f7",
                "warning": "#ffb822",
                "danger": "#fd3995"
            },
            "base": {
                "label": [
                    "#c5cbe3",
                    "#a1a8c3",
                    "#3d4465",
                    "#3e4466"
                ],
                "shape": [
                    "#f0f3ff",
                    "#d9dffa",
                    "#afb4d4",
                    "#646c9a"
                ]
            }
        }
    };
</script>
<script src="{{asset('assets/js/wow.min.js')}}" type="text/javascript"></script>
<script>
    new WOW().init();
</script>
<script src="{{asset('assets/plugins/global/plugins.bundle.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/scripts.bundle.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/pages/custom/login/login-general.js')}}" type="text/javascript"></script>
</body>
</html>
