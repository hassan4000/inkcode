<?php

return [
    'users_management' => 'Users Management',
    'request_management' => 'Request Management',
    'category_management' => 'Category Management',
    'products_management' => 'Products Management',
    'brands_management' => 'Brands Management',
    'wrap_management' => 'Wraps Management',
    'sliders_management' => 'Sliders Management',
    'occasions_management' => 'Occasions Management',
    'relations_management' => 'Relations Management',
     'ads_management' => 'Ads Management',
    'about_app' => 'About App',
    'privacy_policy' => 'Privacy Policy',
    'login' => 'Login',
    'app' => 'Cadeau Boutique',
    'home_page' => 'Home page',
];
