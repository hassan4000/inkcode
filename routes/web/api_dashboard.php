<?php

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['role:admin','auth:web']], function () {
    Route::get('/testapi',[\App\Http\Controllers\Api\Backend\UserController::class,'index']);
    Route::get('get/categories/{id}/sub', [\App\Http\Controllers\Api\Category\CategoryController::class, 'getSubCategory']);

});

