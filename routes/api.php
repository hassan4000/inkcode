<?php

use App\Http\Controllers\Api\Auth\AuthController;
use Illuminate\Support\Facades\Route;

//################################Home Page routes#######################################################################
Route::prefix('auth')->group(function () {
    Route::post('app/login', [AuthController::class, 'login']);
    Route::post('app/admin/login', [AuthController::class, 'loginAdmin']);
    Route::post('app/register', [AuthController::class, 'register']);
    Route::post('app/social', [AuthController::class, 'social']);
    Route::post('app/check/data', [AuthController::class, 'checkData']);
    Route::middleware('auth:api')->get('app/user', [AuthController::class, 'user']);
    Route::middleware('auth:api')->get('app/logout', [AuthController::class, 'logout']);
});

//############################################ Settings Routes #########################################################
Route::group(['prefix' => 'setting', 'middleware' => 'auth:api'], function () {
    Route::post('/change/password', [\App\Http\Controllers\Api\Auth\AuthController::class, 'changePassword']);
    Route::post('/edit/details', [\App\Http\Controllers\Api\Auth\AuthController::class, 'updateDetails']);
    Route::post('/edit/info', [\App\Http\Controllers\Api\Auth\AuthController::class, 'updateInfo']);
});
//#######################################################################################################################

//############################################ Orders Routes #########################################################
Route::group(['middleware' => 'auth:api', 'prefix' => 'order'], function () {



    Route::get('/history', [\App\Http\Controllers\Api\OrderController::class, 'getMyOrders']);
    Route::get('/list/{status}', [\App\Http\Controllers\Api\OrderController::class, 'getOrders']);
    Route::get('/info', [\App\Http\Controllers\Api\OrderController::class, 'getFullInformation']);
    Route::post('/add/item', [\App\Http\Controllers\Api\OrderController::class, 'addItemToOrder']);
    Route::post('/delete/item', [\App\Http\Controllers\Api\OrderController::class, 'removeItemFromOrderOrder']);
    Route::post('/checkout', [\App\Http\Controllers\Api\OrderController::class, 'checkOut']);



});
//#######################################################################################################################
