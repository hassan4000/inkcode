<?php

use App\Http\Controllers\Backend\Order\OrderController;
use Illuminate\Support\Facades\Route;
use Maatwebsite\Excel\Facades\Excel;


Route::group(['middleware' => 'language'], function () {
    Route::group(['middleware' => 'auth'], function () {
        /* begin */
        Route::group(['middleware' => 'role:admin', 'prefix' => 'users', 'as' => 'users.'], function () {
            Route::get('/get/{user_type}', [\App\Http\Controllers\Backend\User\UserController::class, 'index'])->name('index');

            Route::get('/create/{user_type}', [\App\Http\Controllers\Backend\User\UserController::class, 'create'])->name('create');

            Route::post('/store', [\App\Http\Controllers\Backend\User\UserController::class, 'store'])->name('store');

            Route::get('/edit/{id}', [\App\Http\Controllers\Backend\User\UserController::class, 'edit'])->name('edit');

            Route::post('/update/{id}', [\App\Http\Controllers\Backend\User\UserController::class, 'update'])->name('update');

            Route::get('/delete/{id}', [\App\Http\Controllers\Backend\User\UserController::class, 'destroy'])->name('delete');

            Route::post('/change/password/{id}', [\App\Http\Controllers\Backend\User\UserController::class, 'changePassword'])->name('change.password');

            Route::get('/info/{id}', [\App\Http\Controllers\Backend\User\UserController::class, 'show'])->name('show');
        });
################################################################    Orders ######################################################
        Route::group(['middleware' => 'role:admin', 'prefix' => 'orders', 'as' => 'orders.'], function () {
            Route::get('/all', [\App\Http\Controllers\Backend\Order\OrderController::class, 'index'])->name('index');

            Route::get('/create', [\App\Http\Controllers\Backend\Order\OrderController::class, 'create'])->name('create');

            Route::post('/store', [\App\Http\Controllers\Backend\Order\OrderController::class, 'store'])->name('store');
            Route::get('/delete/{id}', [\App\Http\Controllers\Backend\Order\OrderController::class, 'destroy'])->name('delete');

            Route::get('/edit/{id}', [\App\Http\Controllers\Backend\Order\OrderController::class, 'edit'])->name('edit');

            Route::post('/update/{id}', [\App\Http\Controllers\Backend\Order\OrderController::class, 'update'])->name('update');

            Route::post('/order/export/excel', [OrderController::class,'generateExcel'])->name('generateExcel');

            Route::get('/order/item/delete/{id}', [OrderController::class,'destroyOrderItem'])->name('items.delete');
            Route::get('/order/info/{order_id}', [OrderController::class,'InfoOrder'])->name('info');
            //
        });

        #########################################################################################################################
        Route::group(['middleware' => 'role:admin', 'prefix' => 'categories', 'as' => 'categories.'], function () {
            Route::get('/all', [\App\Http\Controllers\Backend\Category\CategoryController::class, 'index'])->name('index');

            Route::get('/create', [\App\Http\Controllers\Backend\Category\CategoryController::class, 'create'])->name('create');

            Route::post('/store', [\App\Http\Controllers\Backend\Category\CategoryController::class, 'store'])->name('store');
            Route::get('/delete/{id}', [\App\Http\Controllers\Backend\Category\CategoryController::class, 'destroy'])->name('delete');

            Route::get('/edit/{id}', [\App\Http\Controllers\Backend\Category\CategoryController::class, 'edit'])->name('edit');

            Route::post('/update/{id}', [\App\Http\Controllers\Backend\Category\CategoryController::class, 'update'])->name('update');
        });
        Route::group(['middleware' => 'role:admin', 'prefix' => 'brands', 'as' => 'brand.'], function () {
            Route::get('/all', [\App\Http\Controllers\Backend\Brand\BrandController::class, 'index'])->name('index');

            Route::get('/create', [\App\Http\Controllers\Backend\Brand\BrandController::class, 'create'])->name('create');

            Route::post('/store', [\App\Http\Controllers\Backend\Brand\BrandController::class, 'store'])->name('store');
            Route::get('/delete/{id}', [\App\Http\Controllers\Backend\Brand\BrandController::class, 'destroy'])->name('delete');

            Route::get('/edit/{id}', [\App\Http\Controllers\Backend\Brand\BrandController::class, 'edit'])->name('edit');

            Route::post('/update/{id}', [\App\Http\Controllers\Backend\Brand\BrandController::class, 'update'])->name('update');
        });

        Route::group(['middleware' => 'role:admin', 'prefix' => 'product', 'as' => 'product.'], function () {
            Route::get('/all', [\App\Http\Controllers\Backend\Product\ProductController::class, 'index'])->name('index');
            Route::get('/create', [\App\Http\Controllers\Backend\Product\ProductController::class, 'create'])->name('create');
            Route::post('/store', [\App\Http\Controllers\Backend\Product\ProductController::class, 'store'])->name('store');
            Route::get('/delete/{id}', [\App\Http\Controllers\Backend\Product\ProductController::class, 'destroy'])->name('delete');
        });

        Route::group(['middleware' => 'role:admin', 'prefix' => 'aboutApp', 'as' => 'aboutApp.'], function () {
            Route::get('/', [\App\Http\Controllers\Backend\AboutApp\AboutAppController::class, 'index'])->name('index');

            Route::get('/edit', [\App\Http\Controllers\Backend\AboutApp\AboutAppController::class, 'edit'])->name('edit');

            Route::post('/update', [\App\Http\Controllers\Backend\AboutApp\AboutAppController::class, 'update'])->name('update');
        });

        Route::group(['middleware' => 'role:admin', 'prefix' => 'privacyPolicy', 'as' => 'privacyPolicy.'], function () {
            Route::get('/', [\App\Http\Controllers\Backend\PrivacyPolicy\PrivacyPolicyController::class, 'index'])->name('index');

            Route::get('/edit', [\App\Http\Controllers\Backend\PrivacyPolicy\PrivacyPolicyController::class, 'edit'])->name('edit');

            Route::post('/update', [\App\Http\Controllers\Backend\PrivacyPolicy\PrivacyPolicyController::class, 'update'])->name('update');
        });

        Route::group(['middleware' => 'role:admin'], function () {
            Route::get('/', [\App\Http\Controllers\Backend\Dashboard\DashboardController::class, 'index'])->name('dashboard');

            Route::post('/switchLang', [\App\Http\Controllers\BaseController::class, 'changeLangWeb'])->name('app.change.lang');
        });

        Route::get('/logout', [\App\Http\Controllers\Backend\Auth\AuthController::class, 'logout'])->name('logout');




    });

    Route::group(['middleware' => 'guest'], function () {
        Route::get('/login', [\App\Http\Controllers\Backend\Auth\AuthController::class, 'showLoginForm'])->name('PageLogin');

        Route::post('login', [\App\Http\Controllers\Backend\Auth\AuthController::class, 'login'])->name('login');

        if (!auth()) {
            Route::get('{any}', function () {
                return redirect(\route('PageLogin'));
            })->where('any', '.*');
        }
    });
});
